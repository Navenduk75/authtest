﻿namespace AuthTest.Domain
{
    public class Baby
    {
        Food StomachFood { get; set; }

        public void Eat(Food food)
        {
            StomachFood = food;
        }

        public Food WhatisInMyStomach()
        {
            return StomachFood;
        }
    }
}
