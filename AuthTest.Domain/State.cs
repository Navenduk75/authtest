﻿namespace AuthTest.Domain
{
    public class State
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
