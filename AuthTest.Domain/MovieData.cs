﻿using System;

namespace AuthTest.Domain
{
    public class MovieData
    {
        public string name { get; set; }
        public string actors { get; set; }
        public string linkParameter { get; set; }
        public string posterUrl { get; set; }
        public Int16 rating { get; set; }
    }
}
