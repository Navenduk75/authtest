﻿using AuthTest.Domain;
using System.Collections.Generic;
using System.Web.Http;

namespace AuthTest.API.Controllers
{
    public class BiharController : ApiController
    {
        // GET: api/Bihar
        public IEnumerable<string> Get()
        {

            Baby chandan = new Baby();

            Food food1 = new Food();
            food1.Name = "Mango";

            chandan.Eat(food1);
            //chandan..StomachFood = "Mango";
            chandan.WhatisInMyStomach();

            return new string[] { "value1", "value2" };
        }

        // GET: api/Bihar/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Bihar
        public string Post([FromBody]Data data)
        {
            if (data.age < 5 || data.age > 10)
            {
                return "Invalid age";
            }
            var a = data.age;
            return "";
        }

        // PUT: api/Bihar/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Bihar/5
        public void Delete(int id)
        {
        }
    }
}
