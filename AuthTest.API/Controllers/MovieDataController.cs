﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Threading;
using System.Configuration;
using AuthTest.DataAccessLayer;
using AuthTest.Domain;

namespace AuthTest.API.Controllers
{

    //    Class/Object
    //      Properties
    //      Methods

    //    Object oriented programming construct:
    //    1. Inheritance
    //    2. Encapsulation
    //    3. Abstraction
    //    4. Polymorphism

    

    //[RoutePrefix("api/movie")]
    [Authorize]
    public class MovieDataController : ApiController
    {

        string connStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        DAL dal = new DAL();

        // GET: api/movie/getAMovie/5
        //[HttpGet]
        //[Route("getAMovie")]
        public MovieData Get(int id)
        {
            Thread.Sleep(1000);

            var email = RequestContext.Principal.Identity.Name;            
            MovieData data = new MovieData();
            try
            {
                data = dal.GetMovieData(connStr, id);
            }
            catch (Exception ex)
            {

            }

            return data;
        }


        // GET: api/movie/allMovies
        //[HttpGet]
        //[Route("allMovies")]
        public List<MovieData> Get()
        {
            Thread.Sleep(1000);

            List<MovieData> allData = new List<MovieData>();

            try
            {                
                allData = dal.GetAllMovieData(connStr);
            }
            catch(Exception ex)
            {
                
            }

            return allData;
        }


        // POST: api/movie/addMovie
        //[HttpPost]
        //[Route("addMovie")]
        public bool Post([FromBody]MovieData data)
        {
            return dal.InsertMovie(connStr, data);
        }

                
        // PUT: api/movie/updateMovie/8
        //[HttpPut]
        //[Route("updateMovie")]
        public bool Put(int id, [FromBody]MovieData data)
        {
            return true;
        }


        // DELETE: api/movie/deleteMovie/3
        //[HttpDelete]
        //[Route("deleteMovie")]
        public bool Delete(int id)
        {
            return true;
        }

    }
}
