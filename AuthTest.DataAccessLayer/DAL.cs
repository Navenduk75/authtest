﻿using AuthTest.Domain;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace AuthTest.DataAccessLayer
{
    public class DAL
    {
        public MovieData GetMovieData(string connStr, int id)
        {
            string query = string.Format("select * from MovieData where ID = {0}", id);

            MovieData data = new MovieData();
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                using (SqlCommand comm = new SqlCommand(query, conn))
                {
                    conn.Open();
                    using (SqlDataReader reader = comm.ExecuteReader())
                    {                        
                        while (reader.Read())
                        {
                            data.name = reader[1].ToString();
                            data.actors = reader[2].ToString();
                            data.linkParameter = reader[3].ToString();
                            data.posterUrl = reader[4].ToString();
                            data.rating = (Int16)reader[5];
                        }                        
                    }
                    conn.Close();
                }
            }
                
            return data;
        }

        public List<MovieData> GetAllMovieData(string connStr)
        {
            List<MovieData> allData = new List<MovieData>();

            SqlConnection conn = new SqlConnection(connStr);
            SqlCommand comm = new SqlCommand("select [Name], Actors,LinkParameter,PosterUrl, Rating from MovieData", conn);
            conn.Open();
            SqlDataReader reader = comm.ExecuteReader();
            while (reader.Read())
            {
                MovieData data = new MovieData();
                data.name = reader[0].ToString();
                data.actors = reader[1].ToString();
                data.linkParameter = reader[2].ToString();
                data.posterUrl = reader[3].ToString();
                data.rating = (Int16)reader[4];

                allData.Add(data);
            }
            conn.Close();
            return allData;
        }

        public bool InsertMovie(string connStr, MovieData data)
        {
            string query = String.Format("insert into MovieData([Name], Actors, LinkParameter, PosterUrl)" +
                " values('{0}','{1}','{2}','{3}')", data.name, data.actors, data.linkParameter, data.posterUrl);
            try
            {
                SqlConnection conn = new SqlConnection(connStr);
                SqlCommand comm = new SqlCommand(query, conn);
                conn.Open();
                comm.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}